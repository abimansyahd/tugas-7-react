import {
    FETCH_PRODUCT_SUCCESS,
    FETCH_PRODUCT_LOADING,
    FETCH_PRODUCT_ERROR,
    DELETE_PRODUCT,
    FETCH_PRODUCT_BY_ID_SUCCESS
} from "../actionTypes/index"

const initialState = {
    product: [],
    oneProduct: {},
    productLoading: true,
    productError: null
}

export default function productReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_PRODUCT_SUCCESS:
            return {
                ...state,
                product: action.payload,
            }
        case FETCH_PRODUCT_BY_ID_SUCCESS:
            return {
                ...state,
                oneProduct: action.payload,
            }
        case FETCH_PRODUCT_LOADING:
            return {
                ...state,
                productLoading: action.payload,
            }
        case FETCH_PRODUCT_ERROR:
            return {
                ...state,
                productError: action.payload,
            }
        case DELETE_PRODUCT:
            const newProduct = state.product.filter((e) => e.id !== action.payload)
            return {
                ...state,
                product: newProduct,
            }
        default:
            return state;

    }

}