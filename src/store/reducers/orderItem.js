import {
  FETCH_ORDERITEM_SUCCESS,
  FETCH_ORDERITEM_LOADING,
  FETCH_ORDERITEM_ERROR,
  DELETE_ORDERITEM,
  FETCH_ORDERITEM_BY_ID_SUCCESS
} from "../actionTypes/index"

const initialState = {
  orderItem: [],
  oneOrderItem: {},
  orderItemLoading: true,
  orderItemError: null
}

export default function orderItemReducer(state = initialState, action) {
  switch (action.type) {
      case FETCH_ORDERITEM_SUCCESS:
          return {
              ...state,
              orderItem: action.payload,
          }
      case FETCH_ORDERITEM_BY_ID_SUCCESS:
          return {
              ...state,
              oneOrderItem: action.payload,
          }
      case FETCH_ORDERITEM_LOADING:
          return {
              ...state,
              orderLoadingItem: action.payload,
          }
      case FETCH_ORDERITEM_ERROR:
          return {
              ...state,
              orderItemError: action.payload,
          }
      case DELETE_ORDERITEM:
          const newOrderItem = state.orderItem.filter((e) => e.id !== action.payload)
          return {
              ...state,
              orderItem: newOrderItem,
          }
      default:
          return state;

  }

}