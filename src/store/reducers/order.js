import {
  FETCH_ORDER_SUCCESS,
  FETCH_ORDER_LOADING,
  FETCH_ORDER_ERROR,
  DELETE_ORDER,
  FETCH_ORDER_BY_ID_SUCCESS
} from "../actionTypes/index"

const initialState = {
  order: [],
  oneOrder: {},
  orderLoading: true,
  orderError: null
}

export default function orderReducer(state = initialState, action) {
  switch (action.type) {
      case FETCH_ORDER_SUCCESS:
          return {
              ...state,
              order: action.payload,
          }
      case FETCH_ORDER_BY_ID_SUCCESS:
          return {
              ...state,
              oneOrder: action.payload,
          }
      case FETCH_ORDER_LOADING:
          return {
              ...state,
              orderLoading: action.payload,
          }
      case FETCH_ORDER_ERROR:
          return {
              ...state,
              orderError: action.payload,
          }
      case DELETE_ORDER:
          const newOrder = state.order.filter((e) => e.id !== action.payload)
          return {
              ...state,
              order: newOrder,
          }
      default:
          return state;

  }

}