// Fetch product
export const FETCH_PRODUCT_SUCCESS = "product/fetchSuccess"
export const FETCH_PRODUCT_LOADING = "product/fetchLoading"
export const FETCH_PRODUCT_ERROR = "product/fetchError"
export const FETCH_PRODUCT_BY_ID_SUCCESS = "product/fetchByIdSuccess"
// Post Edit Delete Product
export const POST_PRODUCT = "product/postProduct"
export const EDIT_PRODUCT = "product/editProduct"
export const DELETE_PRODUCT = "product/deleteProduct"

// Fetch order
export const FETCH_ORDER_SUCCESS = "order/fetchSuccess"
export const FETCH_ORDER_LOADING = "order/fetchLoading"
export const FETCH_ORDER_ERROR = "order/fetchError"
export const FETCH_ORDER_BY_ID_SUCCESS = "order/fetchByIdSuccess"

// Post Edit Delete Order
export const POST_ORDER = "order/postOrder"
export const EDIT_ORDER = "order/editOrder"
export const DELETE_ORDER = "order/deleteOrder"

// Fetch order
export const FETCH_ORDERITEM_SUCCESS = "orderItem/fetchSuccess"
export const FETCH_ORDERITEM_LOADING = "orderItem/fetchLoading"
export const FETCH_ORDERITEM_ERROR = "orderItem/fetchError"
export const FETCH_ORDERITEM_BY_ID_SUCCESS = "orderItem/fetchByIdSuccess"
// Post Edit Delete Order
export const POST_ORDERITEM = "orderItem/postOrderItem"
export const EDIT_ORDERITEM = "orderItem/editOrderItem"
export const DELETE_ORDERITEM = "orderItem/deleteOrderItem"