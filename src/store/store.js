
import {applyMiddleware, createStore, combineReducers} from "redux";
import thunk from 'redux-thunk'
import orderReducer from "./reducers/order";
import orderItemReducer from "./reducers/orderItem";
import productReducer from "./reducers/product";

const rootReducer = combineReducers({
  orderReducer,
  orderItemReducer,
  productReducer
})

const store = createStore(rootReducer, applyMiddleware(thunk))

export default store;