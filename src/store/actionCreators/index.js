import {
  FETCH_PRODUCT_SUCCESS,
  FETCH_PRODUCT_LOADING,
  FETCH_PRODUCT_ERROR,
  FETCH_PRODUCT_BY_ID_SUCCESS,
  DELETE_PRODUCT,
  FETCH_ORDER_SUCCESS,
  FETCH_ORDER_LOADING,
  FETCH_ORDER_ERROR,
  FETCH_ORDER_BY_ID_SUCCESS,
  DELETE_ORDER,
  FETCH_ORDERITEM_SUCCESS,
  FETCH_ORDERITEM_LOADING,
  FETCH_ORDERITEM_ERROR,
  FETCH_ORDERITEM_BY_ID_SUCCESS,
  DELETE_ORDERITEM

} from "../actionTypes/index"

const baseUrl = "http://localhost:3000"

export const fetchProductSuccess = (payload) => {
  return {
    type:FETCH_PRODUCT_SUCCESS,
    payload,
  }
}

export const fetchProductLoading = (payload) => {
  return {
    type: FETCH_PRODUCT_LOADING,
    payload,
  }
}

export const fetchProductError = (payload) => {
  return {
    type: FETCH_PRODUCT_ERROR,
    payload
  }
}

export const fetchProduct = () => {
    
  return (dispatch) => {
    fetch(`${baseUrl}/product/list`)
    .then((resp) => {
      if(resp.ok) {
        return resp.json();
      }
      throw new Error("Something Went Wrong")
    })
    .then((resp) => dispatch(fetchProductSuccess(resp)))
    .catch((err) => dispatch(fetchProductError(err)))
    .finally(() => dispatch(fetchProductLoading(false)))
  }
}

export const findProductByIdSuccess = (payload) => {
  return {
    type: FETCH_PRODUCT_BY_ID_SUCCESS,
    payload
  }
}

export const fetchProductById = (id) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/products/${id}`)
      .then((resp) =>{
        if(resp.ok) {
          return resp.json();
        } else {
          throw new Error("Something Went Wrong");
        }   
      })
      .then((data) => {
        dispatch(findProductByIdSuccess(data))
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

export const postProduct = (payload) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/products`, {
        method:'post',
        body: JSON.stringify(payload)
      })
      .then((resp) =>{
        if(resp.ok) {
          return resp.json()
        } else {
          return resp.json()
          .then((err)=> {
            return Promise.reject(err);
          })
        }   
      })
      .then((data) => {
        dispatch(fetchProduct())
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

export const removeProduct = (id) => {
  return {
    type: DELETE_PRODUCT,
    payload: id
  }
}

export const deleteProduct = (id) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/products/${id}`, {
        method:'delete',
        headers: { 'Content-Type': 'application/json' }
      })
      .then((resp) =>{
        if(resp.ok) {
          return resp.json()
        } else {
          return resp.json()
          .then((err)=> {
            return Promise.reject(err);
          })
        }   
      })
      .then((data) => {
        dispatch(removeProduct(id))
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

export const editProduct = (payload, id) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/products/${id}`, {
        method:'put',
        body: JSON.stringify(payload),
        headers: {
          "Content-Type": "application/json",
        }
      })
      .then((resp) =>{
        if(resp.ok) {
          return resp.json()
        } else {
          return resp.json()
          .then((err)=> {
            return Promise.reject(err);
          })
        }   
      })
      .then((data) => {
        dispatch(fetchProduct())
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

// Order
export const fetchOrderSuccess = (payload) => {
  return {
    type:FETCH_ORDER_SUCCESS,
    payload,
  }
}

export const fetchOrderLoading = (payload) => {
  return {
    type: FETCH_ORDER_LOADING,
    payload,
  }
}

export const fetchOrderError = (payload) => {
  return {
    type: FETCH_ORDER_ERROR,
    payload
  }
}

export const fetchOrder = () => {
    
  return (dispatch) => {
    fetch(`${baseUrl}/order/#############`)
    .then((resp) => {
      if(resp.ok) {
        return resp.json();
      }
      throw new Error("Something Went Wrong")
    })
    .then((resp) => dispatch(fetchOrderSuccess(resp)))
    .catch((err) => dispatch(fetchOrderError(err)))
    .finally(() => dispatch(fetchOrderLoading(false)))
  }
}

export const findOrderByIdSuccess = (payload) => {
  return {
    type: FETCH_ORDER_BY_ID_SUCCESS,
    payload,
  };
}

export const fetchOrderById = (id) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/order/${id}`)
      .then((resp) =>{
        if(resp.ok) {
          return resp.json();
        } else {
          throw new Error("Something Went Wrong");
        }   
      })
      .then((data) => {
        dispatch(findOrderByIdSuccess(data))
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

export const postOrder = (payload) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/order/#################`, {
        method:'post',
        body: JSON.stringify(payload)
      })
      .then((resp) =>{
        if(resp.ok) {
          return resp.json()
        } else {
          return resp.json()
          .then((err)=> {
            return Promise.reject(err);
          })
        }   
      })
      .then((data) => {
        dispatch(fetchOrder())
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

export const removeOrder = (id) => {
  return {
    type: DELETE_ORDER,
    payload: id
  }
}

export const deleteOrder = (id) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/order/${id}`, {
        method:'delete',
      })
      .then((resp) =>{
        if(resp.ok) {
          return resp.json()
        } else {
          return resp.json()
          .then((err)=> {
            return Promise.reject(err);
          })
        }   
      })
      .then((data) => {
        dispatch(removeOrder(id))
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

//OrderItem
export const fetchOrderItemSuccess = (payload) => {
  return {
    type:FETCH_ORDERITEM_SUCCESS,
    payload,
  }
}

export const fetchOrderItemLoading = (payload) => {
  return {
    type: FETCH_ORDERITEM_LOADING,
    payload,
  }
}

export const fetchOrderItemError = (payload) => {
  return {
    type: FETCH_ORDERITEM_ERROR,
    payload
  }
}

export const fetchOrderItem = () => {
    
  return (dispatch) => {
    fetch(`${baseUrl}/orderItem/#############`)
    .then((resp) => {
      if(resp.ok) {
        return resp.json();
      }
      throw new Error("Something Went Wrong")
    })
    .then((resp) => dispatch(fetchOrderItemSuccess(resp)))
    .catch((err) => dispatch(fetchOrderItemError(err)))
    .finally(() => dispatch(fetchOrderItemLoading(false)))
  }
}

export const findOrderItemByIdSuccess = (payload) => {
  return {
    type: FETCH_ORDERITEM_BY_ID_SUCCESS,
    payload,
  };
}

export const fetchOrderItemById = (id) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/orderItem/${id}`)
      .then((resp) =>{
        if(resp.ok) {
          return resp.json();
        } else {
          throw new Error("Something Went Wrong");
        }   
      })
      .then((data) => {
        dispatch(findOrderItemByIdSuccess(data))
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

export const postOrderItem = (payload) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/orderItem/#################`, {
        method:'post',
        body: JSON.stringify(payload)
      })
      .then((resp) =>{
        if(resp.ok) {
          return resp.json()
        } else {
          return resp.json()
          .then((err)=> {
            return Promise.reject(err);
          })
        }   
      })
      .then((data) => {
        dispatch(fetchOrder())
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}

export const removeOrderItem = (id) => {
  return {
    type: DELETE_ORDERITEM,
    payload: id
  }
}

export const deleteOrderItem = (id) => {
  return(dispatch) => {
    return new Promise((resolve, reject)=> {
      fetch(`${baseUrl}/orderItem/${id}`, {
        method:'delete',
      })
      .then((resp) =>{
        if(resp.ok) {
          return resp.json()
        } else {
          return resp.json()
          .then((err)=> {
            return Promise.reject(err);
          })
        }   
      })
      .then((data) => {
        dispatch(removeOrderItem(id))
        resolve(data)
      })
      .catch((err)=> {
        reject(err)
      })
    })
  }
}




