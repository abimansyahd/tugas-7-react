import {Route, Routes} from 'react-router-dom'
import Addproduct from './pages/Addproduct';
import Homepage from './pages/Homepages'
import Editproduct from './pages/Editproduct';

function App() {
  return (
    <div className="App">
      <Routes>
          <Route path = '/' element = {<Homepage/>}/>
          <Route path = '/add-product' element = {<Addproduct/>}/>
          <Route path = '/edit-product/:id' element = {<Editproduct/>}/>
      </Routes>
    </div>
  );
}

export default App;
