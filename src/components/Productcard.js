import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { fetchProductById, deleteProduct } from "../store/actionCreators/index";

export default function Productcard({ data }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  


  const deleteProductButton= (id) => {
    dispatch(deleteProduct(id))
  };

  const toEditPage = (id) => {
    
      dispatch(fetchProductById(id))
      .then(() => {
        navigate(`/edit-product/${id}`);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div
      className="card"
      style={{
        backgroundColor: "green",
        width: "25%",
        height: "300px",
        padding: "20px",
        margin: "20px",
        borderRadius: "10px",
      }}
    >
      <img
        src="https://m.media-amazon.com/images/M/MV5BMjc2NjYyMzgtMmExMi00YzllLTgxNjgtNjA4MmUzMWZlNDZkXkEyXkFqcGdeQXRyYW5zY29kZS13b3JrZmxvdw@@._V1_.jpg"
        alt="Avatar"
        style={{
          width: "100%",
          borderTopLeftRadius: "12px",
          borderTopRightRadius: "12px",
        }}
      />
      <div style={{ flexDirection: "column" }}>
        <h4>
          <b>{data.productName}</b>
        </h4>
        <p>{data.price}</p>
        <p>{data.code}</p>
        <button onClick={() => deleteProductButton(data.id)}>Delete</button>
        <button onClick={() => toEditPage(data.id)}>Edit</button>
      </div>
    </div>
  );
}
