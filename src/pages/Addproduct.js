import React, { useState} from 'react'
import {useNavigate} from "react-router-dom"
import axios from "axios"

export default function Addproduct() {
  const navigate = useNavigate()

  const [product, setProduct] = useState({
    code: '',
    productName: '',
    type: '',
    price: 0
  })

  const onChangeProduct = (e) => {
    const { value } = e.target
    const field = e.target.name

    setProduct({
      ...product,
      [field]: value
    })
  }


  const addProductSubmit = (e) => {
    e.preventDefault()
    const productObj = {
      code: product.code,
      productName: product.productName,
      type: product.type,
      price: +product.price
    };
    axios.post('http://localhost:3000/products', productObj)
    .then(() => {
      navigate("/")
    
    }).catch(err => {
      console.log(err);
    })
      
  }
  return (
    <div style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column"
    }}>
      <div>
        <h1>Add Product</h1>
      </div>
      <form style={{ flexDirection: "column", display: "flex", width: "65%" }}>
        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <label>Code : </label>
          <input 
          type="text" 
          name='code'
          placeholder="Code"
          value={setProduct.code}
          onChange={onChangeProduct}
          />
        </div>

        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <label>Name : </label>
          <input 
          type="text" 
          name='productName'
          placeholder="Name"
          value={setProduct.productName}
          onChange={onChangeProduct} 
          />
        </div>

        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <label>Type : </label>
          <input 
          type="text" 
          name='type'
          placeholder="Type"
          value={setProduct.type}
          onChange={onChangeProduct} 
          />
        </div>

        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <label>Price : </label>
          <input 
          name='price'
          type="number" 
          placeholder="Price"
          value={setProduct.price}
          onChange={onChangeProduct} 
          />
        </div>

        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <input type="submit" onClick={addProductSubmit}></input>
        </div>
      </form>
    </div>

  )
}
