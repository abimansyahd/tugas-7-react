import React, { useEffect, useState } from 'react'
import Productcard from '../components/Productcard'
import axios from 'axios'
import {useNavigate} from 'react-router-dom'

export default function Homepages() {
    const navigate = useNavigate()
    function toAddProduct (){
        navigate("/add-product")
    }
    const [data, setData] = useState([])
    const fetchData = async () => {
        try {
            // const response = await axios.get("http://localhost:8787/products");
            const response = await axios.get("http://localhost:3000/products");
            setData(response.data)
        } catch (error) {
            console.log(error);
        }
    }
    useEffect(() => {
        fetchData()
    }, [])
    return (
        <div>
            <div style={{
                backgroundColor: 'blue',
                width: "100%",
                height: "100%",
                
            }}>

                <h1>Homepages</h1>
                <button onClick={toAddProduct}>Add Product</button>
                <div style={{ display: "flex", flexWrap:"wrap"}}>
                    {
                        data.map((e) => {
                            return (
                                <Productcard
                                    key={e.id}
                                    data={e}
                                />
                            )
                        })

                    }
                </div>
            </div >
        </div>
    )
}
