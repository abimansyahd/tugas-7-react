import React, { useState } from 'react'
import { useNavigate } from "react-router-dom"
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios'
import { editProduct } from '../store/actionCreators'


export default function Editproduct() {
  const navigate = useNavigate()
  const dispatch = useDispatch()


  const { oneProduct } = useSelector((state) => state.productReducer)
  const [editProductForm, setEditProductForm] = useState({
    code: oneProduct.code,
    productName: oneProduct.productName,
    type: oneProduct.type,
    price: oneProduct.price
  })

  const onChangeProduct = (e) => {
    const { value } = e.target
    const field = e.target.name

    setEditProductForm({
      ...editProductForm,
      [field]: value
    })
  }

  const productObj = {
    code: editProductForm.code,
    productName: editProductForm.productName,
    type: editProductForm.type,
    price: editProductForm.price
  }

  const editProductButton = (e) => {
    e.preventDefault();
    dispatch(editProduct(productObj, oneProduct.id))
      .then(() => {
        navigate('/')
      })
      .catch((err) => {
        console.log(err);
      })
  }


  return (
    <div style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column"
    }}>
      <div>
        <h1>Edit Product</h1>
      </div>
      <form style={{ flexDirection: "column", display: "flex", width: "65%" }}>
        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <label>Code : </label>
          <input
            type="text"
            name='code'
            placeholder="Code"
            value={setEditProductForm.code}
            onChange={onChangeProduct}
          />
        </div>

        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <label>Name : </label>
          <input
            type="text"
            name='productName'
            placeholder="Name"
            value={setEditProductForm.productName}
            onChange={onChangeProduct}
          />
        </div>

        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <label>Type : </label>
          <input
            type="text"
            name='type'
            placeholder="Type"
            value={setEditProductForm.type}
            onChange={onChangeProduct}
          />
        </div>

        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <label>Price : </label>
          <input
            name='price'
            type="number"
            placeholder="Price"
            value={setEditProductForm.price}
            onChange={onChangeProduct}
          />
        </div>

        <div style={{ marginBottom: "10px", width: "100%", display: "flex", flexDirection: "column" }}>
          <input
            type="submit"
            onClick={editProductButton}
          >

          </input>
        </div>
      </form>
    </div>
  )
}

